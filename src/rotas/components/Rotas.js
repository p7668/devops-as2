import { BrowserRouter, Route, Routes } from "react-router-dom";
import Login from "../../pages/Login";
import Cadastro from "../../pages/Cadastro";
import AreaLogada from "../../pages/AreaLogada";
import NaoEncontrado from "../../pages/404/component/404";

export default function Rotas() {
    return (
        <BrowserRouter>
            <Routes>
                <Route exact={true} path="/" element={<AreaLogada />} />
                <Route exact={true} path="/cadastro" element={<Cadastro />} />
                <Route exact={true} path="/login" element={<Login />} />
                <Route exact={true} path="/arealogada" element={<AreaLogada />} />
                <Route path="*" element={<NaoEncontrado />} />
            </Routes>   
        </BrowserRouter>
    )
}