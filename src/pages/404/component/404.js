import React from 'react';
import { Link } from 'react-router-dom';

export default function NaoEncontrado() {
    return (
		<div className="main-container">

			<div className='content-container'>
				<h1>404 - Página não encontrada!</h1>

				<Link to="/">Voltar para a página principal</Link>
			</div>
		</div>
	)
}