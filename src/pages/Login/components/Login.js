import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import db from '../../../api/Firebase';
import BannerDeErro from '../../../components/BannerDeErro/components/BannerDeErro';

import '../styles/login.css';

export default function Login() {
	const [credenciais, setCredenciais] = useState({
		email: '',
		senha: ''
	})
	const [erro, setErro] = useState('')

	const navigate = useNavigate()

	const mudouInput = evento => {
		setCredenciais({
		  ...credenciais,
		  [evento.target.id]: evento.target.value
		});
	  }

	function logar(evento) {
		evento.preventDefault();

		db.auth().signInWithEmailAndPassword(credenciais.email, credenciais.senha)
			.then(() => navigate('/arealogada'))
			.catch(error => setErro(error.message))
	}

	return (
		<div className="main-container">

			{ erro && <BannerDeErro mensagem={erro} />}

			<div className='content-container'>
				<h1>Login</h1>

				<div className='inputs-container'>
					<form onSubmit={logar}>
						<input type="email" id="email" placeholder="E-mail" onChange={mudouInput} required />
						<input type="password" id="senha" placeholder="Senha" onChange={mudouInput} required />

						<button type="submit">Acessar</button>
					</form>
				</div>

				<Link to='/cadastro'>Não possuo cadastro</Link>
			</div>
		</div>
	)
}