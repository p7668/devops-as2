import React, { useState, useEffect } from 'react';
import db from '../../../api/Firebase';
import { useNavigate } from 'react-router-dom';

import '../styles/area-logada.css';

export default function AreaLogada() {
	const [dadosUsuario, setDadosUsuario] = useState()

	const navigate = useNavigate()

	useEffect(() => {
		db.auth().onAuthStateChanged(usuarioAtual => {
			if (!usuarioAtual) navigate('/login')
			else {
				db.firestore().collection('usuarios').where('email', '==', usuarioAtual?.email).get()
					.then(res => {
						res.forEach(doc => setDadosUsuario(doc.data()))
					})
					.catch(error => console.log(error))
			}
		})
	})

	function logout() {
		db.auth().signOut()
		navigate('/login');
	}

	return (
		<div className="main-container">

			<div className='content-container'>
				<h1>Dados do usuário</h1>

				<p><span>Nome completo:</span> { `${dadosUsuario?.nome} ${dadosUsuario?.sobrenome}` }</p>
				<p><span>Data de nascimento:</span> { dadosUsuario?.dataNascimento }</p>
				<p><span>E-mail:</span> { dadosUsuario?.email }</p>
				<p><span>Senha:</span> { dadosUsuario?.senha }</p>
				<p><span>uid:</span> { dadosUsuario?.uid }</p>

				<button onClick={logout}>Logout</button>
			</div>
		</div>
	)
}