import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import db from '../../../api/Firebase';
import BannerDeErro from '../../../components/BannerDeErro';

export default function Cadastro() {
  const [dadosUsuario, setDadosUsuario] = useState({
    email: '',
    senha: '',
    nome: '',
    sobrenome: '',
    dataNascimento: ''
  });
  const [erro, setErro] = useState();

  const navigate = useNavigate()

  const cadastrar = evento => {
    evento.preventDefault();

    db.auth().createUserWithEmailAndPassword(dadosUsuario.email, dadosUsuario.senha)
      .then((res) => {
        db.firestore().collection("usuarios").add({
          ...dadosUsuario,
          uid: res.user.uid
        })
      })
      .then(() => navigate('/'))
      .catch(error => setErro(error.message))
  }

  const mudouInput = evento => {
    setDadosUsuario({
      ...dadosUsuario,
      [evento.target.id]: evento.target.value
    });
  }


  return (
    <div className="main-container">

      { erro && <BannerDeErro mensagem={erro} /> }

      <div className='content-container'>
        <h1>Cadastro</h1>

        <div className='inputs-container'>
          <form onSubmit={cadastrar}>
            <label htmlFor="email">E-mail</label>
            <input type="email" id="email" placeholder="exemplo@dominio.com.br" onChange={mudouInput} required />

            <label htmlFor="senha">Senha</label>
            <input type="password" id="senha" placeholder="*********" onChange={mudouInput} required />

            <label htmlFor="nome">Nome</label>
            <input type="text" id="nome" placeholder="João" onChange={mudouInput} required />

            <label htmlFor="sobrenome">Sobrenome</label>
            <input type="text" id="sobrenome" placeholder="Silva" onChange={mudouInput} required />

            <label htmlFor="dataNascimento">Data de Nascimento</label>
            <input type="date" id="dataNascimento" onChange={mudouInput} required />

            <button type="submit">Cadastrar</button>
          </form>
        </div>

        <Link to='/Login'>Já possuo cadastro</Link>
      </div>
    </div>
  )
}