import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const firebaseConfig = {
  apiKey: "AIzaSyA8LYPWyxAnhJiOhnhKOGWT_aVijMGp6Vc",
  authDomain: "dev-web-as2.firebaseapp.com",
  projectId: "dev-web-as2",
  storageBucket: "dev-web-as2.appspot.com",
  messagingSenderId: "169719220074",
  appId: "1:169719220074:web:b6ec78977031afad458713",
  measurementId: "G-Q6GQ936YSZ"
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export default firebase;