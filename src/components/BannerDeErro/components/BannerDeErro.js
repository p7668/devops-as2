import React from 'react';

import '../styles/banner-de-erro.css';

export default function BannerDeErro({ mensagem }) {
    return (
        <div className="erro-container">
            { mensagem }
        </div>
    )
}